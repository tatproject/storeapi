﻿using System;
using System.Collections.Generic;

#nullable disable

namespace StoreDB.storeModels
{
    public partial class Inventory
    {
        public Inventory()
        {
            InvoiceDetails = new HashSet<InvoiceDetail>();
            TransactionDetails = new HashSet<TransactionDetail>();
        }

        public int InventoryId { get; set; }
        public string CodeCategory { get; set; }
        public int SupplierId { get; set; }
        public string Img { get; set; }
        public string Description { get; set; }
        public string Barcode { get; set; }
        public int Price { get; set; }
        public DateTime DateRecieved { get; set; }
        public DateTime DateRemoved { get; set; }
        public string Size { get; set; }
        public string Color { get; set; }
        public string Quantity { get; set; }

        public virtual Supplier Supplier { get; set; }
        public virtual ICollection<InvoiceDetail> InvoiceDetails { get; set; }
        public virtual ICollection<TransactionDetail> TransactionDetails { get; set; }
    }
}
