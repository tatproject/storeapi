﻿using System;
using System.Collections.Generic;

#nullable disable

namespace StoreDB.storeModels
{
    public partial class Invoice
    {
        public Invoice()
        {
            InvoiceDetails = new HashSet<InvoiceDetail>();
        }

        public int InvoiceId { get; set; }
        public int? InvoiceNumber { get; set; }
        public int? SupplierId { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public int? SumToPay { get; set; }
        public int? CodePayment { get; set; }
        public int? SumPayed { get; set; }
        public DateTime? PaymentDate { get; set; }

        public virtual Code CodePaymentNavigation { get; set; }
        public virtual Supplier Supplier { get; set; }
        public virtual ICollection<InvoiceDetail> InvoiceDetails { get; set; }
    }
}
