﻿using System;
using System.Collections.Generic;

#nullable disable

namespace StoreDB.storeModels
{
    public partial class Code
    {
        public Code()
        {
            Invoices = new HashSet<Invoice>();
            Suppliers = new HashSet<Supplier>();
            Transactions = new HashSet<Transaction>();
        }

        public int Code1 { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Invoice> Invoices { get; set; }
        public virtual ICollection<Supplier> Suppliers { get; set; }
        public virtual ICollection<Transaction> Transactions { get; set; }
    }
}
