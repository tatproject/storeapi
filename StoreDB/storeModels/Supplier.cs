﻿using System;
using System.Collections.Generic;

#nullable disable

namespace StoreDB.storeModels
{
    public partial class Supplier
    {
        public Supplier()
        {
            Inventories = new HashSet<Inventory>();
            Invoices = new HashSet<Invoice>();
        }

        public int SupplierId { get; set; }
        public string SupplierName { get; set; }
        public string Zone { get; set; }
        public string Address { get; set; }
        public int? CityCode { get; set; }
        public string Telephone { get; set; }
        public string ContactName { get; set; }

        public virtual Code CityCodeNavigation { get; set; }
        public virtual ICollection<Inventory> Inventories { get; set; }
        public virtual ICollection<Invoice> Invoices { get; set; }
    }
}
