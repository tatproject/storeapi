﻿using System;
using System.Collections.Generic;

#nullable disable

namespace StoreDB.storeModels
{
    public partial class Transaction
    {
        public Transaction()
        {
            TransactionDetails = new HashSet<TransactionDetail>();
        }

        public int TransactionId { get; set; }
        public DateTime? TransactionDate { get; set; }
        public int? CustomerId { get; set; }
        public int? SumPayed { get; set; }
        public int? CodePayment { get; set; }
        public int? Discount { get; set; }

        public virtual Code CodePaymentNavigation { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual ICollection<TransactionDetail> TransactionDetails { get; set; }
    }
}
