﻿using System;
using System.Collections.Generic;

#nullable disable

namespace StoreDB.storeModels
{
    public partial class InvoiceDetail
    {
        public int InvoiceDetaild { get; set; }
        public int? InvoiceId { get; set; }
        public int? InventoryId { get; set; }
        public int? Amount { get; set; }

        public virtual Inventory Inventory { get; set; }
        public virtual Invoice Invoice { get; set; }
    }
}
