﻿using System;
using System.Collections.Generic;

#nullable disable

namespace StoreDB.storeModels
{
    public partial class TransactionDetail
    {
        public int TransactionDetailId { get; set; }
        public int? TransactionId { get; set; }
        public int? InventoryId { get; set; }
        public int? Amount { get; set; }

        public virtual Inventory Inventory { get; set; }
        public virtual Transaction Transaction { get; set; }
    }
}
