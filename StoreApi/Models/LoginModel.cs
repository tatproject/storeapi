﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StoreApi.Models
{
    public class LoginModelrequest
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
    public class LoginModelresponse
    {
        public string ResponseCode { get; set; }
        public string ResponseMessage { get; set; }
    }
}
