﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StoreApi.Models
{
    public class StoreItemsModelResponse
    {
        public List<StoreItemsModel> storeItemsModels { get; set; }
        public string ResponseCode { get; set; }
        public string ResponseMessage { get; set; }
        public StoreItemsModelResponse()
        {
            this.storeItemsModels = new List<StoreItemsModel>();
        }

    }

        public class StoreItemsModel
    {
        public string ProdactName { get; set; }
        public string ProdactId { get; set; }
        public string Compeny { get; set; }
        public string color { get; set; }
        public string size { get; set; }
        public string Code { get; set; }
        public string description { get; set; }
        public string price { get; set; }
        public string quantity { get; set; }
        public string image { get; set; }
        public string category { get; set; }

    }
    

}
