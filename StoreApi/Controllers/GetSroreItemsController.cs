﻿using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using StoreApi.Models;
using StoreDB.storeContext;
using StoreDB.storeModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StoreApi.Controllers
{
    [ApiController]
    public class GetSroreItemsController : ControllerBase
    {
        [Route("GetSroreItems")]
        [HttpPost]
        public IActionResult Post()
        {
            
            StoreItemsModelResponse response = new StoreItemsModelResponse();
            response.storeItemsModels =  GetItems();
            return Ok(response);
        }

        private List<StoreItemsModel> GetItems()
        {
            List<StoreItemsModel> storeItems = new List<StoreItemsModel>();
            List<Inventory> inventory = new List<Inventory>();
            using (storeContext db = new storeContext())
            {
                var id = 1;
                var query =
                   from i in db.Inventories
                   join c in db.Codes on i.CodeCategory equals c.Code1.ToString()
                   select new { category = c.Description, i.Color, i.Barcode, i.Description, i.Img, i.InventoryId, i.Price, i.Size,
                    i.Quantity, i.TransactionDetails };
                foreach (var item in query)
                {
                    storeItems.Add(new StoreItemsModel { category = item.category, Code = item.Barcode, color = item.Color });
                }
            }
           

            // return inventory;
            return storeItems;
        }
    }
        
    }

